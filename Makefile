all: rateit

rateit_glade.h: rateit.glade
	cat rateit.glade | perl -e 'print "static unsigned char rateit_glade[]=";while(<>){s/"/\\"/g; s/^/"/; s/$$/\\n"/; print}print ";\n"' > rateit_glade.h

rateit: rateit.c rateit_glade.h
	gcc -g -O2 rateit.c -o rateit `pkg-config --cflags --libs libglade-2.0` -export-dynamic

clean:
	rm rateit rateit_glade.h
