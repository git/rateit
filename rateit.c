#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>

#define MAX_SAMPLES 10
#define MAX_TRIALS 80

#include "rateit_glade.h"

typedef struct {
   char *name;
   char *path;
   int id;
   int pos;
   int score;
   char *comment;
} Sample;

typedef struct Trial {
   int id;
   int nbSamples;
   char *name;
   int refID;
   Sample *samp;
} Trial;

typedef struct {
   int active;
   char *basepath;
   char *participant;
   int nbSamples;
   int nbTrials;
   char *experiment_name;
   char *savepath;
   Trial *trials;
   int curr_trial;
   int last_played;
} Session;

Session *session = NULL;

GladeXML  *main_window;

GtkWidget *vscales[MAX_SAMPLES];
GtkWidget *commentButtons[MAX_SAMPLES];
GtkWidget *playButtons[MAX_SAMPLES];

pid_t play_pid=0;

void chld_handler(int sig)
{
   pid_t pid;
   fprintf(stderr, "Got SIGCHLD\n");
   while ((pid=waitpid(-1, NULL, WNOHANG))>0)
   {
      if (pid==play_pid)
         play_pid = 0;
   }
   play_pid = 0;
}

int xmlGetInt(xmlNode *node, const char *prop, int def)
{
   xmlChar *tmpStr;
   int val = def;
   tmpStr = xmlGetProp(node, (const xmlChar*)prop);
   if (tmpStr)
   {
      val = atoi((char*)tmpStr);
      xmlFree(tmpStr);
   }
   return val;
}

char *xmlGetString(xmlNode *node, const char *prop, char *def)
{
   xmlChar *tmpStr;
   char *val = def;
   tmpStr = xmlGetProp(node, (const xmlChar*)prop);
   if (tmpStr)
   {
      val = strdup((char*)tmpStr);
      xmlFree(tmpStr);
   }
   return val;
}

void free_session()
{
   /* FIXME: LEAK HERE -- Must delete a few here if we don't want to leak */
}

void destroy_session()
{
   /* FIXME: BIG LEAK HERE -- Must delete lots of stuff here if we don't want to leak */
   session=NULL;
}

/*int pos2ID(int pos)
{
   int id, i;
   Trial *tr = &session->trials[session->curr_trial];
   for (i=0;i<session->nbSamples;i++)
   {
      if (tr->samp[i].pos == ID)
         return tr->samp[i].id;
   }
   return -1;
}*/

void save_session()
{
   int i,j;
   xmlDocPtr doc = NULL;       /* document pointer */
   xmlNodePtr root_node = NULL, trial_node = NULL, sample_node = NULL; /* node pointers */
   /*xmlDtdPtr dtd = NULL;*/       /* DTD pointer */
   char buf[256];
   printf ("Saving to %s\n", session->savepath);
   doc = xmlNewDoc(BAD_CAST "1.0");
   root_node = xmlNewNode(NULL, BAD_CAST "MUSHRAEval");
   xmlDocSetRootElement(doc, root_node);
   
   snprintf(buf, 256, "%d", session->curr_trial);
   xmlNewProp(root_node, BAD_CAST "currTrial", BAD_CAST buf);
   xmlNewProp(root_node, BAD_CAST "expName", BAD_CAST session->experiment_name);

   for (i=0;i<session->nbTrials;i++)
   {
      trial_node = xmlNewChild(root_node, NULL, BAD_CAST "SessionTrial", NULL);
      snprintf(buf, 256, "%d", i);
      xmlNewProp(trial_node, BAD_CAST "id", BAD_CAST buf);
      for (j=0;j<session->nbSamples;j++)
      {
         sample_node = xmlNewChild(trial_node, NULL, BAD_CAST "PlayedSample", BAD_CAST session->trials[i].samp[j].comment);
         snprintf(buf, 256, "%d", session->trials[i].samp[j].id);
         xmlNewProp(sample_node, BAD_CAST "id", BAD_CAST buf);
         snprintf(buf, 256, "%d", session->trials[i].samp[j].pos);
         xmlNewProp(sample_node, BAD_CAST "pos", BAD_CAST buf);
         snprintf(buf, 256, "%d", session->trials[i].samp[j].score);
         xmlNewProp(sample_node, BAD_CAST "score", BAD_CAST buf);
      }
   }
   xmlSaveFormatFileEnc(session->savepath, doc, "UTF-8", 1);

   /*free the document */
   xmlFreeDoc(doc);
}

void on_save_as_activate (GtkWidget *widget, gpointer user_data)
{
   GtkWidget *saveasDialog = glade_xml_get_widget (main_window, "SaveAsDialog");
   /*
   if (!session->savepath)
   {
   gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (saveasDialog), ".");
   gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (saveasDialog), "Untitled session");
}
   else
   gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (saveasDialog), session->savepath);
   */
   if (gtk_dialog_run (GTK_DIALOG (saveasDialog)) == GTK_RESPONSE_OK)
   {
      char *filename;
      filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (saveasDialog));
      session->savepath = strdup(filename);
      save_session();
      g_free (filename);
   } else {
      printf ("Cancel open\n");
   }
   gtk_widget_hide(saveasDialog);
}

void updateGUI()
{
   int i;
   GtkWidget *w;
   const char *expStr, *trialStr="";
   if (session)
   {
      for (i=0;i<session->nbSamples;i++)
      {
         gtk_range_set_value (GTK_RANGE(vscales[session->trials[session->curr_trial].samp[i].pos]), 
                              session->trials[session->curr_trial].samp[i].score);
         /*gtk_widget_set_sensitive(vscales[session->trials[session->curr_trial].samp[i].pos],session->active);
         gtk_widget_set_sensitive(commentButtons[session->trials[session->curr_trial].samp[i].pos],session->active);
         gtk_widget_set_sensitive(playButtons[session->trials[session->curr_trial].samp[i].pos],session->active);*/
         gtk_widget_set_sensitive(vscales[session->trials[session->curr_trial].samp[i].pos],session->last_played==session->trials[session->curr_trial].samp[i].id);
         gtk_widget_set_sensitive(commentButtons[session->trials[session->curr_trial].samp[i].pos],session->last_played==session->trials[session->curr_trial].samp[i].id);
      }
      expStr = session->experiment_name;
      if (session->active)
         trialStr=session->trials[session->curr_trial].name;
   } else {
      expStr = "No experiment loaded";
   }
   
   w = glade_xml_get_widget (main_window, "labelExperiment");
   gtk_label_set_text(GTK_LABEL(w), expStr);
   w = glade_xml_get_widget (main_window, "labelTrial");
   gtk_label_set_text(GTK_LABEL(w), trialStr);

   w = glade_xml_get_widget (main_window, "new1");
   gtk_widget_set_sensitive(w, session!=NULL);
   w = glade_xml_get_widget (main_window, "open1");
   gtk_widget_set_sensitive(w, session!=NULL);
   w = glade_xml_get_widget (main_window, "save1");
   gtk_widget_set_sensitive(w, session!=NULL && session->active);
   w = glade_xml_get_widget (main_window, "save_as1");
   gtk_widget_set_sensitive(w, session!=NULL && session->active);
   
   w = glade_xml_get_widget (main_window, "table1");
   gtk_widget_set_sensitive(w, session!=NULL && session->active);
   w = glade_xml_get_widget (main_window, "buttonNext");
   gtk_widget_set_sensitive(w, session!=NULL && session->active);
   if (session!=NULL && session->active)
   {
      if (session->curr_trial == session->nbTrials-1)
      {
         w = glade_xml_get_widget (main_window, "labelNext");
         gtk_label_set_markup(GTK_LABEL(w), "<b>Finish</b>");
      } else {
         w = glade_xml_get_widget (main_window, "labelNext");
         gtk_label_set_markup(GTK_LABEL(w), "<b>Next trial</b>");
      }
   }
   w = glade_xml_get_widget (main_window, "buttonPrev");
   gtk_widget_set_sensitive(w, session!=NULL && session->active && session->curr_trial != 0);
   w = glade_xml_get_widget (main_window, "buttonPlayRef");
   gtk_widget_set_sensitive(w, session!=NULL && session->active);
   w = glade_xml_get_widget (main_window, "buttonPause");
   gtk_widget_set_sensitive(w, session!=NULL && session->active && play_pid);
   w = glade_xml_get_widget (main_window, "buttonStop");
   gtk_widget_set_sensitive(w, session!=NULL && session->active && play_pid);
}

void on_save_activate (GtkWidget *widget, gpointer user_data)
{
   if (session->savepath)
      save_session();
   else
      on_save_as_activate(NULL, NULL);
}


int close_request()
{
   GtkWidget *dialog = glade_xml_get_widget (main_window, "CloseConfirm");
   gint response = gtk_dialog_run (GTK_DIALOG (dialog));
   gtk_widget_hide(dialog);
   if (response == GTK_RESPONSE_YES)
   {
      on_save_activate(NULL, NULL);
      return 1;
   } else if (response == GTK_RESPONSE_NO)
   {
      return 1;
   } else
   {
      return 0;
   }
}

void new_session()
{
   int i,j;
   if (!session)
      return;
   if (session->active)
   {
      /* Close current session */
      if (!close_request())
         return;
      free_session();
   }
   session->active = 1;
   session->curr_trial = 0;
   for (i=0;i<session->nbTrials;i++)
   {
      for (j=0;j<session->nbSamples;j++)
      {
         int unique;
         session->trials[i].samp[j].score = 100;
         do
         {
            unique = 1;
            int k;
            /* FIXME: Must randomize properly */
            session->trials[i].samp[j].pos = rand()%session->nbSamples;
            for (k=0;k<j;k++)
            {
               if (session->trials[i].samp[k].pos == session->trials[i].samp[j].pos)
               {
                  unique = 0;
                  break;
               }
            }
         } while (!unique);
      }
   }
   printf ("New session\n");
   updateGUI();
}

int create_session(xmlDocPtr current_experiment)
{
   xmlNode *root;
   xmlNode *trial_node;
   xmlNode *sample_node;
   xmlChar *tmpStr;
   int i,j;
   int corrupted = 0;
   
   session = malloc(sizeof(Session));
   session->active = 0;
   root = xmlDocGetRootElement(current_experiment);

   tmpStr = xmlGetProp(root, (const xmlChar*)"nbSamples");
   if (tmpStr)
   {
      int i;
      session->nbSamples = atoi((char*)tmpStr);
      xmlFree(tmpStr);
   } else {
      session->nbSamples = 0;
   }

   tmpStr = xmlGetProp(root, (const xmlChar*)"nbTrials");
   if (tmpStr)
   {
      session->nbTrials = atoi((char*)tmpStr);
      xmlFree(tmpStr);
   } else {
      session->nbTrials = 0;
   }
   if (session->nbTrials < 1 || session->nbTrials>= MAX_TRIALS || session->nbSamples<1 || session->nbSamples>=MAX_SAMPLES)
   {
      fprintf (stderr, "Attempted to create a session with %d trials and %d samples\n", session->nbTrials, session->nbSamples);
      free(session);
      session = NULL;
      return 0;
   }
   for (i=0;i<session->nbSamples;i++)
   {
      gtk_widget_show(vscales[i]);
      gtk_widget_show(commentButtons[i]);
      gtk_widget_show(playButtons[i]);
   }
   for (i=session->nbSamples;i<MAX_SAMPLES;i++)
   {
      gtk_widget_hide(vscales[i]);
      gtk_widget_hide(commentButtons[i]);
      gtk_widget_hide(playButtons[i]);
   }

   session->last_played = -1;
   session->experiment_name = xmlGetString(root, "name", "unknown experiment");
   session->trials = malloc(sizeof(Trial)*session->nbTrials);
   session->participant = NULL;
   session->savepath = NULL;
   session->curr_trial = 0;
   session->basepath = xmlGetString(root, "samplePath", "");
   for (i=0;i<session->nbTrials;i++)
   {
      session->trials[i].samp = malloc(sizeof(Session)*session->nbSamples);
      session->trials[i].id = i;
      session->trials[i].refID = 0;
      session->trials[i].name = NULL;
      session->trials[i].nbSamples = session->nbSamples;
      for (j=0;j<session->nbSamples;j++)
      {
         session->trials[i].samp[j].score = 100;
         session->trials[i].samp[j].id = j;
         session->trials[i].samp[j].pos = j;
         session->trials[i].samp[j].comment = NULL;
         session->trials[i].samp[j].name = NULL;
         session->trials[i].samp[j].path = NULL;
      }
   }

   trial_node = root->children;
   while (trial_node)
   {
      if (trial_node->type == XML_ELEMENT_NODE)
      {
         int tid=0;
         tid = xmlGetInt(trial_node, "id", 0);
         if (tid >= session->nbTrials || tid <0)
         {
            fprintf (stderr, "Attempted to load trial %d of %d\n", tid, session->nbTrials);
            trial_node = trial_node->next;
            corrupted = 1;
            continue;
         }
         /*fprintf (stderr, "tid = %d of %d\n", tid, session->nbTrials);*/
         session->trials[tid].name = xmlGetString(trial_node, "name", NULL);
         sample_node = trial_node->children;
         while (sample_node)
         {
            if (trial_node->type == XML_ELEMENT_NODE && strcmp((char*)sample_node->name, "Sample")==0)
            {
               char *tmpStr;
               int sid = xmlGetInt(sample_node, "id", 0);
               if (sid >= session->nbSamples || sid <0)
               {
                  fprintf (stderr, "Attempted to load sample %d of %d\n", sid, session->nbSamples);
                  sample_node = sample_node->next;
                  corrupted = 1;
                  continue;
               }
               session->trials[tid].samp[sid].name = xmlGetString(sample_node, "name", NULL);
               /*session->trials[tid].samp[sid].path = xmlGetString(sample_node, "file", NULL);*/
               tmpStr = (char*)xmlGetProp(sample_node, (xmlChar*)"file");
               if (tmpStr)
               {
                  session->trials[tid].samp[sid].path = malloc(strlen(tmpStr)+strlen(session->basepath)+2);
                  strcpy(session->trials[tid].samp[sid].path, session->basepath);
                  strcat(session->trials[tid].samp[sid].path, "/");
                  strcat(session->trials[tid].samp[sid].path, tmpStr);
               }
               tmpStr = xmlGetString(sample_node, "ref", NULL);
               if (tmpStr && strcmp(tmpStr, "true")==0)
                  session->trials[tid].refID = sid;
               free(tmpStr);
               session->trials[tid].samp[sid].id = sid;
            }
            sample_node = sample_node->next;
         }
      }
      trial_node = trial_node->next;
   }
   for (i=0;i<session->nbTrials;i++)
   {
      for (j=0;j<session->nbSamples;j++)
      {
         if (!session->trials[i].samp[j].path)
         {
            fprintf (stderr, "Sample has no path\n");
            corrupted = 1;
            break;
         }
      }
   }
   if (corrupted)
   {
      for (i=0;i<session->nbTrials;i++)
         free(session->trials[i].samp);
      fprintf (stderr, "Loading of experiment failed (bad file)\n");
      free(session);
      session = NULL;      
      return 0;
   }
   return 1;
}


void loadSession(const char *filename)
{
   xmlDocPtr current_session;
   xmlNode *session_root;
   xmlNode *trial_node;
   xmlChar *sname;
   if (!session)
      return;
   if (session->active)
   {
      /* Close current session */
      if (!close_request())
         return;
      free_session();
   }
   session->savepath = strdup(filename);
   current_session = xmlReadFile(filename, NULL, 0);
   if (current_session == NULL) {
      fprintf(stderr, "Failed to parse %s\n", filename);
      return;
   } else {
      fprintf(stderr, "Successfully loaded %s\n", filename);
   }
   session_root = xmlDocGetRootElement(current_session);
   sname = xmlGetProp(session_root, (const xmlChar*)"expName");
   if (!sname || strcmp((char*)sname, session->experiment_name) != 0)
   {
      fprintf(stderr, "Session name doesn't match: %s\n", filename);
      if (sname) 
         xmlFree(sname);
      xmlFreeDoc(current_session);
      return;
   }
   xmlFree(sname);
   session->curr_trial = xmlGetInt(session_root, "currTrial", 0);
   trial_node = session_root->children;
   while (trial_node)
   {
      if (trial_node->type == XML_ELEMENT_NODE)
      {
         int tid;
         xmlNode *sample_node;
         tid = xmlGetInt(trial_node, "id", 0);
         sample_node = trial_node->children;
         while (sample_node)
         {
            if (sample_node->type == XML_ELEMENT_NODE)
            {
               int sid;
               sid = xmlGetInt(sample_node, "id", 0);
               session->trials[tid].samp[sid].pos = xmlGetInt(sample_node, "pos", 0);
               session->trials[tid].samp[sid].score = xmlGetInt(sample_node, "score", 0);
               /*fprintf(stderr, "name: %s at %s\n", session->trials[tid].name, session->trials[tid].samp[sid].path);*/
            }
            sample_node = sample_node->next;
         }
      }
      trial_node = trial_node->next;
   }
   xmlFreeDoc(current_session);
   session->active = 1;
   updateGUI();
}


void loadExperiment(const char *filename)
{
   xmlDocPtr current_experiment;
   xmlNode *root;
   if (session)
   {
      if (!close_request())
         return;
      /* Close current session */
      destroy_session();
   }
   current_experiment = xmlReadFile(filename, NULL, 0);
   if (current_experiment == NULL) {
      fprintf(stderr, "Failed to parse %s\n", filename);
      return;
   } else {
      fprintf(stderr, "Successfully loaded %s\n", filename);
   }
   root = xmlDocGetRootElement(current_experiment);
   if (strcmp((char*)root->name, "MUSHRA") != 0)
   {
      fprintf(stderr, "Failed validate %s\n", filename);
      xmlFreeDoc(current_experiment);
      current_experiment = NULL;
      return;
   }
   create_session(current_experiment);
   updateGUI();
   xmlFreeDoc(current_experiment);
}

int getIDFromLabel(GtkWidget *label)
{
   const char *name = gtk_label_get_text(GTK_LABEL(label));
   if (strcmp(name, "Ref")==0)
      return -1;
   else
      return name[0]-'A';
}


Sample *getSampleFromLabel(GtkWidget *label, int update_last_played)
{
   int i;
   Trial *tr;
   int ID = getIDFromLabel(label);
   /*printf ("pos = %d\n", ID);*/
   tr = &session->trials[session->curr_trial];
   for (i=0;i<session->nbSamples;i++)
   {
      if (tr->samp[i].pos == ID)
      {
         if (update_last_played)
            session->last_played = i;
         return &tr->samp[i];
      }
   }
   return &tr->samp[tr->refID];
}

void on_new_activate (GtkWidget *widget, gpointer user_data)
{
   new_session();
}

void on_open_activate (GtkWidget *widget, gpointer user_data)
{
   GtkWidget *openDialog = glade_xml_get_widget (main_window, "OpenDialog");
   gtk_widget_show(openDialog);
   if (gtk_dialog_run (GTK_DIALOG (openDialog)) == GTK_RESPONSE_OK)
   {
      char *filename;
      filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (openDialog));
      /*open_file (filename);*/
      /*printf ("Opening %s\n", filename);*/
      loadSession(filename);
      g_free (filename);
   }
   gtk_widget_hide(openDialog);
}

void on_open_experiment (GtkWidget *widget, gpointer user_data)
{
   GtkWidget *openDialog = glade_xml_get_widget (main_window, "OpenDialog");
   gtk_widget_show(openDialog);
   if (gtk_dialog_run (GTK_DIALOG (openDialog)) == GTK_RESPONSE_OK)
   {
      char *filename;
      filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (openDialog));
      /*printf ("Opening %s\n", filename);*/
      loadExperiment(filename);
      g_free (filename);
   }
   gtk_widget_hide(openDialog);
}



void on_vscale_value_changed (GtkWidget *widget, gpointer user_data)
{
   Sample *s = getSampleFromLabel(widget, 0);
   s->score = gtk_range_get_value(GTK_RANGE(vscales[s->pos]));
   /*printf ("%d -> %d\n", s->id, s->score);*/
}

void on_buttonPlay_clicked (GtkWidget *widget, gpointer user_data)
{
   Sample *s = getSampleFromLabel(widget, 1);
   /*snprintf(command, 256, "play \"%s\" >& /dev/null &", s->path);*/
   /*printf ("%s\n", command);*/
   /*system("killall play >& /dev/null; killall sox >& /dev/null");*/
   /*system(command);*/
   if (play_pid)
   {
      kill(play_pid, SIGINT);
      waitpid(play_pid, NULL, 0);
   }
   if ((play_pid=fork())==0)
   {
      close(0);
      close(1);
      close(2);
      execlp("sox", "sox", s->path, "-t",  "ossdsp", "/dev/dsp", NULL);
      perror("execlp");
      _exit(0);
   }
   updateGUI();
}

void on_buttonPause_clicked (GtkWidget *widget, gpointer user_data)
{
   /*system("killall play >& /dev/null; killall sox >& /dev/null");*/
   if (play_pid)
   {
      kill(play_pid, SIGINT);
      waitpid(play_pid, NULL, 0);
      printf ("Pause!\n");
   }
   updateGUI();
}

void on_buttonStop_clicked (GtkWidget *widget, gpointer user_data)
{
   /*system("killall play >& /dev/null; killall sox >& /dev/null");*/
   if (play_pid)
   {
      kill(play_pid, SIGINT);
      waitpid(play_pid, NULL, 0);
      printf ("Stop!\n");
   }
   updateGUI();
}

void on_buttonNext_clicked (GtkWidget *widget, gpointer user_data)
{
   if (session->curr_trial < session->nbTrials-1)
   {
      session->curr_trial++;
      session->last_played=-1;
      updateGUI();
      on_buttonStop_clicked(NULL, NULL);
      printf ("Next!\n");
   } else {
      on_save_activate(NULL, NULL);
      printf ("Completed!\n");
   }
}

void on_buttonPrev_clicked (GtkWidget *widget, gpointer user_data)
{
   if (session->curr_trial > 0)
   {
      session->curr_trial--;
      session->last_played=-1;
      updateGUI();
      printf ("Prev!\n");
   } else {
      printf ("Already at the beginning!\n");
   }
}

void on_buttonComment_clicked (GtkWidget *widget, gpointer user_data)
{
   GtkWidget *commentDialog;
   GtkTextIter startIter, endIter;
   GtkTextBuffer *buffer;
   
   Sample *s = getSampleFromLabel(widget, 0);
   
   commentDialog = glade_xml_get_widget (main_window, "CommentDialog");
   GtkWidget *textview = glade_xml_get_widget (main_window, "textviewComments");
   buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
   gtk_text_buffer_set_text(buffer, s->comment ? s->comment : "", -1);
   
   if (gtk_dialog_run (GTK_DIALOG (commentDialog)) == GTK_RESPONSE_CLOSE)
   {
      char *commentStr;
      gtk_text_buffer_get_start_iter(buffer, &startIter);
      gtk_text_buffer_get_end_iter(buffer, &endIter);
      commentStr = gtk_text_buffer_get_text(buffer, &startIter, &endIter, FALSE);
      free(s->comment);
      s->comment = strdup(commentStr);
      g_free(commentStr);
   } else {
      printf ("Cancel open\n");
   }
   gtk_widget_hide(commentDialog);
}

void on_instructions_activate (GtkWidget *widget, gpointer user_data)
{
   GtkWidget *dialog;
   dialog = glade_xml_get_widget (main_window, "InstructionsDialog");
   gtk_dialog_run (GTK_DIALOG (dialog));
   gtk_widget_hide(dialog);
}

void on_hscale_value_changed (GtkWidget *widget, gpointer user_data)
{
}

void quit_request (GtkWidget *widget, gpointer user_data)
{
   if (session && session->active)
   {
      if (!close_request())
         return;
   }
   on_buttonStop_clicked(NULL, NULL);
   gtk_main_quit();
}


void
on_about_activate (GtkWidget *widget, gpointer user_data)
{
   GtkWidget *about = glade_xml_get_widget (main_window, "AboutDialog");
   gtk_widget_show(about);
}

int
main (int argc, char *argv[])
{
   int i;
   srand(time(NULL));
   gtk_init (&argc, &argv);
   LIBXML_TEST_VERSION

   signal (SIGCHLD, chld_handler);
   /* load the interface */
   /*main_window = glade_xml_new ("rateit.glade", NULL, NULL);*/
   main_window = glade_xml_new_from_memory((char*)rateit_glade, 1+strlen((char*)rateit_glade), NULL, NULL);

   /* connect the signals in the interface */
   glade_xml_signal_autoconnect (main_window);

   /* Extract widgets to a table */
   for (i=0;i<MAX_SAMPLES;i++)
   {
      char widget_name[100];
      snprintf(widget_name, 100, "vscale%c", 'A'+i);
      vscales[i] = glade_xml_get_widget (main_window, widget_name);
      snprintf(widget_name, 100, "buttonComment%c", 'A'+i);
      commentButtons[i] = glade_xml_get_widget (main_window, widget_name);
      snprintf(widget_name, 100, "buttonPlay%c", 'A'+i);
      playButtons[i] = glade_xml_get_widget (main_window, widget_name);
   }

   if (argc>=2)
      loadExperiment(argv[1]);
   if (argc>=3)
      loadSession(argv[2]);
   updateGUI();
   /* start the event loop */
   gtk_main ();
   xmlCleanupParser();
   return 0;
}
